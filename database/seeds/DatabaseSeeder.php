<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'demo category1'],    
            ['name' => 'demo category2'],     
            ['name' => 'demo category3'],     
            ['name' => 'demo category4'],     
            ['name' => 'demo category5'],     
            ['name' => 'demo category6'],     

        ];

        foreach ($items as $item) {
            \App\Category::create($item);
        }

        $roles = [
            ['name' => 'admin', 'guard_name' => 'web'],
            ['name' => 'user', 'guard_name' => 'web'],

        ];
        foreach ($roles as $itemx) {
            \App\Role::create($itemx);
        }

        $permission = [
            ['name' => 'add-faq', 'guard_name' => 'web'],
            ['name' => 'edit-faq', 'guard_name' => 'web'],
            ['name' => 'list-faq', 'guard_name' => 'web'],
            ['name' => 'list-user', 'guard_name' => 'web'],
        ];
         foreach ($permission as $itemn) {
            \App\Permission::create($itemn);
        }

        $role_p = [
        	['role_id' => 1, 'permission_id' => 1],
        	['role_id' => 1, 'permission_id' => 2],
        	['role_id' => 1, 'permission_id' => 3],
        	['role_id' => 1, 'permission_id' => 4],
        	['role_id' => 2, 'permission_id' => 1],
        	['role_id' => 2, 'permission_id' => 2],
        	['role_id' => 2, 'permission_id' => 3],

        ];
          foreach ($role_p as $itemn1) {
            DB::table('role_has_permissions')->insert($itemn1);
        }
    }
}
