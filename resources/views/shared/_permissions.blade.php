<div class="panel panel-default no-border-radius">
    <div class="panel-heading" role="tab" id="{{ isset($title) ? str_slug($title) :  'permissionHeading' }}">
        <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#dd-{{ isset($title) ? str_slug($title) :  'permissionHeading' }}" aria-expanded="<php //$closed or ?> {{ 'true' }}" aria-controls="dd-{{ isset($title) ? str_slug($title) :  'permissionHeading' }}">
                <!-- {{ $title or 'Override Permissions' }} {!! isset($user) ? '<span class="text-danger">(' . $user->getDirectPermissions()->count() . ')</span>' : '' !!} -->
                {{ $title }}
            </a>
        </h4>
    </div>
    <div id="dd-{{ isset($title) ? str_slug($title) :  'permissionHeading' }}" class="panel-collapse collapse <?php //$closed or ?> {{ 'in' }}" role="tabpanel" aria-labelledby="dd-{{ isset($title) ? str_slug($title) :  'permissionHeading' }}">
        <div class="panel-body">
            <div class="">
                <div class="form-group col-sm-4" style="padding: 0;">
                    <input type="text" name="role_name" class="role_name_{{ $id }} form-control" value="{{ $ori_title }}" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                @foreach($permissions as $perm)
                    <?php
                        $per_found = null;

                        if( isset($role) ) {
                            $per_found = $role->hasPermissionTo($perm->name);
                        }

                        
                       /* if( isset($user)) {
                            $per_found = $user->hasDirectPermission($perm->name);
                        }*/
                    ?>

                    <div class="col-md-3">
                        <div class="">
                            <label class="{{ str_contains($perm->name, 'delete') ? 'text-danger' : '' }}">
                                {!! Form::checkbox("permissions[]", $perm->name, $per_found, isset($options) ? $options : []) !!} {{ $perm->name }}
                            </label>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    
    <div class="clear"></div>
    <div class="row">
    <div class="role-button-panel">{!! Form::submit('Save', ['class' => 'btn btn-primary margin-top']) !!}</div>
    </div>
    <div class="clear"></div>
</div>