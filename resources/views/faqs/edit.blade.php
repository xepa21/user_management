@extends('adminlte::layouts.app')

@section('htmlheader_title')	
        {{ trans('adminlte_lang::message.useredit') }}
@endsection

@section('main-content')

<?php
$usr->password = "";
?>
	<div class="row">
        <div class="col-md-12 ">
         <h3>{{ trans('adminlte_lang::message.useredit') }}</h3>
            <!-- @foreach ($errors->all() as $error)
        		<p class="alert alert-warning">{{ $error }}</p>
		    @endforeach -->
			<!-- if there are creation errors, they will show here -->	
		    <div class="row">
                <div class="col-xxl-12 col-lg-12 col-xl-12 col-md-12">
		    	{!! Form::model($usr,['url' => '/admin_user/'.$usr->id,  'data-parsley-validate', 'method' => 'PUT', 'files'=>true]) !!}

                 <section class="box-typical box-typical-padding">
            <section>
            <div class="row">
            
            <div class="col-md-6">
            <div class="form-group">
                {!! Form::label(trans('adminlte_lang::message.name'), trans('adminlte_lang::message.name')) !!}						
                {!! Form::text('name', null, array('class' => 'form-control','tabindex' => '1','required' => 'required','data-parsley-required-message' => trans('adminlte_lang::message.name_is_required'))) !!}
                {!! $errors->first('name', '<p class="alert alert-danger">:message</p>') !!}
            </div>

             <div class="form-group">
                {!! Form::label(trans('adminlte_lang::message.email'), trans('adminlte_lang::message.email')) !!}                       
                {!! Form::text('email', null, array('class' => 'form-control','tabindex' => '2','required' => 'required','data-parsley-required-message' => trans('adminlte_lang::message.email_is_required'),'readonly'=>'true')) !!}
                
            </div>

            <div class="form-group">
                {!! Form::label(trans('adminlte_lang::message.password'), trans('adminlte_lang::message.password')) !!}                       
                {!! Form::password('password', array('class' => 'form-control','tabindex' => '3')) !!}
                {!! $errors->first('password', '<p class="alert alert-danger">:message</p>') !!}
            </div>

             <div class="form-group">
                {!! Form::label(trans('adminlte_lang::message.usertype'), trans('adminlte_lang::message.usertype')) !!}                       
                <select id="role" tabindex="4" name="role" class="btn-group bootstrap-select bootstrap-select-arrow" required="required" data-parsley-required-message="{{ trans('adminlte_lang::message.role_is_required') }}">
                <?php
                $sel1 = "";
                $sel2 = "";
                $sel4 = "";
                if($usr->role == "1"){
                    $sel1 = 'selected="selected"';
                }else if($usr->role == "2"){
                    $sel2 = 'selected="selected"';
                }else if($usr->role == "4"){
                    $sel4 = 'selected="selected"';
                }
                ?>
                    <option value="">-{!! trans('adminlte_lang::message.select') !!}-</option>                     
                      <option value="1" <?php echo $sel1; ?>>Admin</option>
                      <option value="2" <?php echo $sel2; ?>>Staff</option>
                      <option value="4" <?php echo $sel4; ?>>Delivery</option>
                </select>
                {!! $errors->first('role', '<p class="alert alert-danger">:message</p>') !!}
            </div> 

             <div class='form-group'>
                {!! Form::label('status', trans('adminlte_lang::message.stutus'))   !!}
                <div class="radio">
                {!! Form::radio('status', '1',true,array('id'=>'active','tabindex' => '5')) !!} 
                <label for="active">{!! trans('adminlte_lang::message.active') !!}</label>
                </div>
                <div class="radio">
                {!! Form::radio('status', '0','',array('id'=>'inactive','tabindex' => '6')) !!} 
                <label for="inactive">{!! trans('adminlte_lang::message.inactive') !!}</label> 
                </div>
            </div> 
            
            </div>

            <div class="col-md-6">
            <div class="form-group">
                {!! Form::label(trans('adminlte_lang::message.country'), trans('adminlte_lang::message.country')) !!}   
                             
                <select id="country" tabindex="7" name="country" class="btn-group bootstrap-select bootstrap-select-arrow" onchange="getbranch(this.value)" required="required" data-parsley-required-message="{{ trans('adminlte_lang::message.country_is_required') }}">
                    <option value="">-{!! trans('adminlte_lang::message.select') !!}-</option>
                    
                    @foreach ($contrydata as $country)
                    <?php
                    $sel = "";
                    if($usr->country_id == $country->country_id){
                        $sel = 'selected="selected"'; 
                    }
                    ?> 
                      <option value="{{ $country->country_id }}" <?php echo $sel; ?>>{{ $country->country_name }}</option>
                    @endforeach
                </select>
                {!! $errors->first('country', '<p class="alert alert-danger">:message</p>') !!}
            </div>
            <div class="form-group">
                {!! Form::label(trans('adminlte_lang::message.user_branch'), trans('adminlte_lang::message.user_branch')) !!}
                
                <select name="branch" class="form-control static-dropdown" tabindex="8" required="required" data-parsley-required-message="{{ trans('adminlte_lang::message.branch_is_required') }}">
                     @foreach ($branchdata as $branch)
                     <?php
                    $sel = "";
                    if($usr->branch_id == $branch->branchId){
                        $sel = 'selected="selected"'; 
                    }
                    ?> 
                        <option value="{{ $branch->branchId }}" <?php echo $sel; ?>>{{ $branch->brnchName }}</option>
                     @endforeach
                    <!-- <option value="{{ $usr->branch_id }}" selected="selected">{{ $usr->branch_id }}</option> -->
                </select>
            </div>
            <div class="form-group">
                {!! Form::label(trans('adminlte_lang::message.phone'), trans('adminlte_lang::message.phone')) !!}                       
                {!! Form::text('phone', null, array('class' => 'form-control','tabindex' => '9','onkeypress' => 'return isNumber(event)','required' => 'required','data-parsley-required-message' => trans('adminlte_lang::message.phone_is_required'))) !!}
                {!! $errors->first('phone', '<p class="alert alert-danger">:message</p>') !!}
            </div>
            </div> 

            <div class="col-md-6">
            </div>

            <div class="col-md-6"> 

             <div class="form-group upload-image-thumbnail">
                    {!! Form::label('Uploadimage', trans('adminlte_lang::message.image'),['class'=>'required']) !!}
                    <!-- -->
                    <input type='file' name="usrImage" id="aa" tabindex = "10" onchange="pressed()" style="display:none;">
                    <label id="fileLabel" for="aa" class="form-control" >{{ trans('adminlte_lang::message.selectimg') }}</label>
                    <!-- -->
                    <!-- {!! Form::file('usrImage', null, array('class' => 'form-control')) !!} -->
                    {!! $errors->first('usrImage', '<p class="alert alert-danger">:message</p>') !!}
					<div class="thumbnail col-md-6"><img src="{{ imageUploadUrl(5,$usr->logo) }}" height="80" /></div>
                </div>
           
            </div>

            <div class="col-md-6">
            <div class="col-xs-6 ">
                    {!! Html::decode(Form::label('role_id', trans('quickadmin.users.fields.role').' <span class="text-danger">*</span>', ['class' => 'control-label'])) !!}
                    {!! Form::select('role_id', $roles, old('role_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('role_id'))
                        <p class="help-block">
                            {{ $errors->first('role_id') }}
                        </p>
                    @endif
                </div>
           
            </div>                    
            
         

            </div>					   

            <section class="proj-page-add-txt table-margin form-buttons">
            <fieldset class="form-group">
            {!! Form::submit(trans('adminlte_lang::message.edit'), array('class' => 'btn btn-primary btn-sm')) !!}
            <a class="btn btn-secondary btn-close btn-sm" href="{{ URL::to('admin_user') }}">{{ trans('adminlte_lang::message.cancel') }}</a>
            <div class="clear"></div>
            </fieldset>
            </section>

            </section>
            </section>

			{!! Form::close() !!}

			    </div>
			    </div>
        </div>                    
	</div>

    
@endsection
<script type="text/javascript">
    window.pressed = function(){
    var a = document.getElementById('aa');
    var lbl = "<?php echo trans('adminlte_lang::message.selectimg') ?>";
    if(a.value == "")
    {
        fileLabel.innerHTML = lbl;
    }
    else
    {
        var theSplit = a.value.split('\\');
        fileLabel.innerHTML = theSplit[theSplit.length-1];
    }
};
</script>
<script type="text/javascript">
// jQuery(document).ready(function() {
//     alert($("#country").val());  
// });
function getbranch(val){
    // var b_id = $("#hidd_branch").val();
    var countryID = val;
    var url = '{{ url('') }}';
    if(countryID) {
         $.ajax({
            url: url+'/ajax/getbranch?country_id=' + countryID 
        }).done(function(data){
            var res = JSON.parse(data);
            $('select[name="branch"]').empty();
            $.each(res, function(key, value) {
                $('select[name="branch"]').append('<option value="'+ value.branchId +'">'+ value.brnchName +'</option>');
            });
        });
    }else{
        $('select[name="branch"]').empty();
    }
}

   

</script>