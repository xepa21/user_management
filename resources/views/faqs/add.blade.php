        <div class="col-md-12 ">   
        <h3>Add</h3>
            <!-- @foreach ($errors->all() as $error)
            <p class="alert alert-warning">{{ $error }}</p>
            @endforeach -->
            <!-- if there are creation errors, they will show here -->
            <div class="row">
            <div class="col-xxl-12 col-lg-12 col-xl-12 col-md-12">			   
            
            {!! Form::open(array('data-parsley-validate',  'files' => true, 'id' => 'faq_form')) !!}

            <section class="box-typical box-typical-padding">
            <section>
            <div class="row">
            
            <div class="col-md-6">
           
                <div class="form-group">
                    {!! Html::decode(Form::label('category_id', 'Category <span class="text-danger">*</span>', ['class' => 'control-label'])) !!}
                    {!! Form::select('category_id', $categories, old('category_id'), ['class' => 'form-control ', 'required' => '', 'id' => 'category_id']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('category_id'))
                        <p class="help-block">
                            {{ $errors->first('category_id') }}
                        </p>
                    @endif
                </div>
             <div class="form-group">
                {!! Form::label('Title', 'Title') !!}                       
                {!! Form::text('title', null, array('class' => 'form-control','required' => 'required','data-parsley-required-message' => 'title is required')) !!}
                {!! $errors->first('title', '<p class="alert alert-danger">:message</p>') !!}
            </div> 
            <div class="form-group">
                {!! Form::label('Description', 'Description') !!}                       
                {!! Form::textarea('description', null, array('class' => 'form-control','required' => 'required','data-parsley-required-message' => 'description is required', 'id' => 'description')) !!}
                {!! $errors->first('description', '<p class="alert alert-danger">:message</p>') !!}
            </div> 
            <div class="form-group">
                {!! Form::label('Image', 'Image') !!}                       
                {!! Form::file('image', array('class' => 'form-control','tabindex' => '3','required' => 'required','data-parsley-required-message' => 'Image is required')) !!}
                <div id="imageBox"></div>

                {!! $errors->first('description', '<p class="alert alert-danger">:message</p>') !!}
            </div>                
            </div>
            </div>					   
            <input type="hidden" name="f_id" id="f_id">
            <section class="proj-page-add-txt table-margin form-buttons">
            <fieldset class="form-group">
            {!! Form::submit('Create', array('class' => 'btn btn-primary btn-sm')) !!}
            <div class="clear"></div>
            </fieldset>
            </section>

            </section>
            </section>

            {!! Form::close() !!}

            </div>
            </div>
                  
    </div>

