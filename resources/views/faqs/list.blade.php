<div class="table-responsive" >
    <table id="datatable1" class="table table-bordered table-striped dataTable">
    <thead>
        <tr>
            <th> Category</th>
            <th> Title</th>
            <th>Description</th>
            <th> Image</th>
            <th style="width: 145px !important;"> Action</th>
        </tr>
    </thead>
    <tbody>
    
    @foreach ($faqs as $faq)  
    <tbody>  
        <tr> 
           <?php
           $categoryname = "";
           $category = App\Category::find($faq->category_id);
           if($category) {
            $categoryname = $category->name;
           }
           ?>
            <td>{{ $categoryname }}</td>
            <td>{{ $faq->title }}</td>
            <td>{{ $faq->description }}</td>                 
            <td><img src="{{ url('/public/uploads/'.$faq->image) }}" height="50px" width="50px" /></td>  
            <td>                
                @can('edit-faq')
                <a href="javascript:void(0);" class="btn btn-info" id="editForm{{$faq->id}}" onclick="editForm('{{$faq->id}}');">Edit</a>
                <a href="javascript:void(0);" onclick="deleteRow('{{$faq->id}}');" class="btn btn-danger">Delete</a>
                @endcan

            </td>

        </tr>
        </tbody>
    @endforeach
    
</tbody>
</table>
{!! $faqs->links() !!}
</div>


