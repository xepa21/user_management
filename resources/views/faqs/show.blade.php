@extends('layouts.app')

@section('content')
<div id="datatable1_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <div class="row">                    
        <div class="container-fluid">
            <h3 class="customer-title">
                FAQs                        
            </h3>
            @if (Session::has('success'))
            <div class="alert alert-success">{!! Session::get('success') !!}</div>
            @endif
        </div>
        
        @can('add-faq')
        @include('faqs/add')
        @endcan

        <section>
            <div class="col-md-6 ">           
                <div class="form-group">
                    {!! Form::label('Search', 'Search') !!}                       

                    {!! Form::text('search', null, array('class' => 'form-control', 'onKeyUp' => 'searchData()', 'id' => 'search')) !!}
                </div>
            </div>
        </section>
        <div class="col-md-12" id="faq_div">                            
                
                @include('faqs/list')
                         
        </div>                    
    </div>
</div>

@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

<script type="text/javascript">

    $(document).ready(function() {
         // alert('fgf');
        $("form#faq_form").submit(function(e) {
            
        e.preventDefault();    
        var formData = new FormData(this);
        console.log(formData);
        $.ajax({
            url: "{{url('/faq')}}",            
            type: 'POST',
            data: formData,
            success: function (data) {
                $(".errors_show").empty();
                if(data.errors) {
                    var errorArr = data.errors;
                    var validations = [];
                    $.each(errorArr, function(ee, ee1) {
                        console.log(ee);
                        validations.push(ee1[0]);
                        validations.push("<br>");

                    });
                    $(".errors_show").show().html(validations);
                    return false;
                }               
                $('#faq_div').empty().html(data);
                document.getElementById("faq_form").reset();
                 $("#f_id").val("");
                 $("#imageBox").empty();
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    
    $(document).on('click', '.pagination a', function(event){
        event.preventDefault(); 
        var page = $(this).attr('href').split('page=')[1];
        fetch_data(page);
    });
    });

    function editForm(id) {
        $.ajax({
            url: "{{url('/faq')}}"+'/'+id+'/'+'edit',
            type: "GET",
            success: function(data) {
                $("#category_id").val(data.category_id);

                $("input[name='title']").val(data.title);
                $("#description").val(data.description);
                $("input[name='image']").attr('required', false);
                $("input[name='description']").attr('required', false);
                $("input[name='title']").attr('required', false);
                $("input[name='category_id']").attr('required', false);


                $("#f_id").val(data.id);
                var imageUrl = "{{url('public/uploads')}}"+'/'+data.image
                 $("#imageBox").html('<img src="' + imageUrl + '" height="50px" width="50px"/>');
               
            }
            
        });
    }

    function deleteRow(id) {
        $.ajax({
            url: "{{url('/faq/delete')}}"+'/'+id,
            type: "GET",
            success: function(data) {
                 $('#faq_div').empty().html(data);
                document.getElementById("faq_form").reset();
            }
            
        });
    }

    function fetch_data(page)
       {
        $.ajax({
            url:"{{url('')}}"+'/faq/fetch_data?page='+page,
            success:function(data)
            {
                $('#faq_div').html(data);
            }
        });
    }

     function searchData() {
        var search = $("#search").val();
        $.ajax({
            url:"{{url('')}}"+'/faq/search/'+search,
            type: "GET",
            success:function(data)
            {                
                $('#faq_div').empty().html(data);
            }
        });
    }
</script>
</script>