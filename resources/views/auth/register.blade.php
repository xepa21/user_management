@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class='form-group row'>
                            <label for="status" class="col-md-4 col-form-label text-md-right">Status</label>
                            <div class="col-md-6">
                            <div class="radio">
                            {!! Form::radio('status', '1',true,array('id'=>'active','tabindex' => '5')) !!} 
                            <label for="active">Active</label>
                            </div>
                            <div class="radio">
                            {!! Form::radio('status', '0','',array('id'=>'inactive','tabindex' => '6')) !!} 
                            <label for="inactive">Inactive</label> 
                            </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mobile_number" class="col-md-4 col-form-label text-md-right">Mobile number</label>

                            <div class="col-md-6">                     
                            {!! Form::number('mobile_number', null, array('class' => 'form-control','tabindex' => '9','onkeypress' => 'return isNumber(event)','required' => 'required','data-parsley-required-message' => 'Mobile number is required')) !!}
                            {!! $errors->first('mobile_number', '<p class="alert alert-danger">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group row">
                            @php
                            $roles = \App\Role::get()->pluck('name', 'id')->prepend('--Select Role--', '');  
                            @endphp
                            <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>
                            

                            <div class="col-md-6">
                            {!! Form::select('role_id', $roles, old('role_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('role_id'))
                                <p class="help-block">
                                    {{ $errors->first('role_id') }}
                                </p>
                            @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
