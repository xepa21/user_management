<div class="table-responsive"> 

<table id="" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th> Unique ID</th>
            <th> Role</th>
            <th>Email</th>
            <th> Phone Number</th>
            <th> Status</th>
            <th style="width: 145px !important;"> Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($usr as $user)  
    <tbody>  
        <tr> 
           
            <td>{{ $user->unique_id }}</td>
            <td>{{ $user->roles()->pluck('name') }}</td>
            <td>{{ $user->email }}</td>                 
            <td>{{ $user->mobile_number }}</td>  

            <td><?php echo $user->status==1 ? '<span class="label label-success">'.'Active'.'</span>' : '<span class="label label-danger">'.'Inactive'.'</span>';?></td>                                  
            <td>
                @if($user->status == 1)
                <a href="{{url('user/status_update/'.$user->id.'/0')}}" class="btn btn-danger">Inactive</a>
                @else
                <a href="{{url('user/status_update/'.$user->id.'/1')}}" class="btn btn-success">Active</a>
                @endif
            </td>

        </tr>
        </tbody>
    @endforeach
</tbody>
</table>
{!! $usr->links() !!}
</div>