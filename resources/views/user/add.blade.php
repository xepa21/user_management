@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12 ">   
        <h3>Add</h3>
            <!-- @foreach ($errors->all() as $error)
            <p class="alert alert-warning">{{ $error }}</p>
            @endforeach -->
            <!-- if there are creation errors, they will show here -->
            <div class="row">
            <div class="col-xxl-12 col-lg-12 col-xl-12 col-md-12">			   
            
            {!! Form::open(array('url' => ('user'), 'data-parsley-validate',  'files' => true,'action'=>'POST')) !!}

            <section class="box-typical box-typical-padding">
            <section>
            <div class="row">
            
            <div class="col-md-6">
           

             <div class="form-group">
                {!! Form::label('Email', 'Email') !!}                       
                {!! Form::text('email', null, array('class' => 'form-control','tabindex' => '2','required' => 'required','data-parsley-required-message' => 'Email is required')) !!}
                {!! $errors->first('email', '<p class="alert alert-danger">:message</p>') !!}
            </div> 
            <div class="form-group">
                {!! Form::label('Password', 'Password') !!}                       
                {!! Form::password('password', array('class' => 'form-control','tabindex' => '3','required' => 'required','data-parsley-required-message' => 'Password is required')) !!}
                {!! $errors->first('password', '<p class="alert alert-danger">:message</p>') !!}
            </div>            

            <div class='form-group'>
                {!! Form::label('status', 'Status')   !!}
                <div class="radio">
                {!! Form::radio('status', '1',true,array('id'=>'active','tabindex' => '5')) !!} 
                <label for="active">Active</label>
                </div>
                <div class="radio">
                {!! Form::radio('status', '0','',array('id'=>'inactive','tabindex' => '6')) !!} 
                <label for="inactive">Inactive</label> 
                </div>
            </div>

             
            </div>

            <div class="col-md-6">           
            
            <div class="form-group">
                {!! Form::label('Mobile number', 'Mobile number') !!}                       
                {!! Form::text('phone', null, array('class' => 'form-control','tabindex' => '9','onkeypress' => 'return isNumber(event)','required' => 'required','data-parsley-required-message' => 'Mobile number is required')) !!}
                {!! $errors->first('phone', '<p class="alert alert-danger">:message</p>') !!}
            </div>
            </div> 
                   
            <div class="col-xs-6 ">
                <div class="form-group">
                    {!! Html::decode(Form::label('role_id', 'Role <span class="text-danger">*</span>', ['class' => 'control-label'])) !!}
                    {!! Form::select('role_id', $roles, old('role_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('role_id'))
                        <p class="help-block">
                            {{ $errors->first('role_id') }}
                        </p>
                    @endif
                </div>
            </div>
            
            </div>					   

            <section class="proj-page-add-txt table-margin form-buttons">
            <fieldset class="form-group">
            {!! Form::submit(trans('adminlte_lang::message.create'), array('class' => 'btn btn-primary btn-sm')) !!}
            <a class="btn btn-secondary btn-close btn-sm" href="{{ URL::to('admin_user') }}">{{ trans('adminlte_lang::message.cancel') }}</a>
            <div class="clear"></div>
            </fieldset>
            </section>

            </section>
            </section>

            {!! Form::close() !!}

            <div id="columns"></div>

            </div>
            </div>
                  
    </div>
</div>

@endsection
<script type="text/javascript">
window.pressed = function(){
    var a = document.getElementById('aa');
    var lbl = "<?php echo trans('adminlte_lang::message.selectimg') ?>";
    if(a.value == "")
    {
        fileLabel.innerHTML =lbl;
    }
    else
    {
        var theSplit = a.value.split('\\');
        fileLabel.innerHTML = theSplit[theSplit.length-1];
    }
};
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
<script type="text/javascript">


   

</script>
