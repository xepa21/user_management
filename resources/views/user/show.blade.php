@extends('layouts.app')

@section('content')
<div id="datatable1_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <div class="row">                    
        <div class="container-fluid">
            <h3 class="customer-title">
                Users                        
            </h3>
            @if (Session::has('success'))
            <div class="alert alert-success">{!! Session::get('success') !!}</div>
            @endif
        </div>
        <section>
            <div class="col-md-6 ">           
                <div class="form-group">
                    {!! Form::label('Search', 'Search') !!}                       

                    {!! Form::text('search', null, array('class' => 'form-control', 'onKeyUp' => 'searchData()', 'id' => 'search')) !!}
                </div>
            </div>
        </section>
        <div class="col-md-12"  id="table_data">                            
                @include('user/list')
                           
        </div>                    
    </div>
</div>

@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
 
<script type="text/javascript">
    $(document).ready(function(){
// alert('f');
       $(document).on('click', '.pagination a', function(event){
        event.preventDefault(); 
        var page = $(this).attr('href').split('page=')[1];
        fetch_data(page);
    });

       function fetch_data(page)
       {
        $.ajax({
            url:"{{url('')}}"+'/users/fetch_data?page='+page,
            success:function(data)
            {
                $('#table_data').html(data);
            }
        });
    }

});

    function searchData() {
        var search = $("#search").val();
        $.ajax({
            url:"{{url('')}}"+'/users/search/'+search,
            type: "GET",
            success:function(data)
            {
                $('#table_data').empty().html(data);
            }
        });
    }
</script>
