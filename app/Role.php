<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 *
 * @package App
 * @property string $title
*/
class Role extends \Spatie\Permission\Models\Role {

    protected $fillable = ['name', 'guard_name'];
    protected $hidden = [];
    
    
    
}
