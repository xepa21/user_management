<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        // Check validation
        $this->validate($request, [
            'mobile_no' => 'required',            
        ]);
        // dd('sdsd');
        // Get user record
        $user = User::where('mobile_number', $request->get('mobile_no'))->where('status', 1)->first();
// dd($user);
        // Check Condition Mobile No. Found or Not
        if(!$user) {
            return redirect()->back()->with('success', 'Your mobile number not match in our system or the user might be blocked!!');
            // return back();
        }        
        // dd('df');
        // Set Auth Details
        Auth::login($user);
        
        // Redirect home page
        return redirect()->route('home');
    }
}
