<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Role;
use App\Permission;
use App\RolePermission;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
            'mobile_number' => ['unique:users']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = new User;

        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->status = $data['status'];
        $user->unique_id = rand(000, 999);            
            $user->mobile_number = $data['mobile_number'];
            $user->role_id = $data['role_id'];
       $user->save();

        return $this->syncPermissions($data,$user);
        // return redirect('home');

    }

    private function syncPermissions($data,$user)
    {
        // Get the submitted roles
        $roles = $data['role_id'];
        $getpermissions = RolePermission::where('role_id', $roles)->get();
        $permissions = [];
        foreach ($getpermissions as $key => $value) {
            $permissions = $value->permission_id;
        }
        // dd($permissions);
        // Get the roles
        $roles = Role::find($roles);

        // check for current role changes
        if( ! $user->hasAllRoles( $roles ) ) { 
            // reset all direct permissions for user
           // $roles->syncPermissions($permissions);
            User::find($user->id)->assignRole($roles);
        } else { 
            User::find($user->id)->assignRole($roles);

            // handle permissions
            // $user->syncPermissions($permissions);
        }

        // $user->syncRoles($roles);
        return $user;
    }
}
