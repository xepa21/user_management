<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Collective\Html\Eloquent\FormAccessible;
use Hash;
use Crypt;
use DB;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $usr = User::paginate(5);      
        return View::make('user/show', array('usr' => $usr));
    }

    function fetch_data(Request $request)
    {
     if($request->ajax())
     { 
      $usr = User::paginate(5);
     // dd($usr);
      return view('user/list', compact('usr'))->render();
     }
    }

    function search($search)
    {
        // dd($search);
         $usr = User::paginate(5);
        if($search != '') {
        $usr = User::where('email', 'like', '%'.$search.'%')->orWhere('mobile_number', 'like', '%'.$search.'%')->orWhere('unique_id', 'like', '%'.$search.'%')->paginate(5);
        }
        // dd($usr);
      return view('user/list', array('usr' => $usr))->render();
     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  

        $User = User::pluck('name','id')->toArray();
        $roles = \App\Model\Role::get()->pluck('name', 'id')->prepend('--Select Role--', '');        
        return view('user.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'mobile_number' => 'required',
            'role' => 'required',
        );
        $message = array(
        'email.required' =>  'Email is required',
        'password.required' =>  'Password is required',
        'phone.required' =>  'Mobile is required',
        'role.required' =>  'Role is required'
        );
        $validator = Validator::make(Input::all(), $rules,$message);
        if ($validator->fails()) { 
            return Redirect::to('users/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {   
            // upload the image //
            /*$file = $request->file('usrImage');
            $destination_path = folderArray(5);
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $file->move($destination_path, $filename);*/

            $password = Input::get('password'); 
            // password is form field     
            $hashed = Hash::make($password);

            $usr = new User;
            $usr->email      = Input::get('email');
            $usr->mobile_number       = Input::get('mobile_number');
            $usr->role      = Input::get('role');
            $usr->status = Input::get('status');
            $usr->save();
            $this->syncPermissions($request, $usr);
            // // redirect    
            return redirect('users')->with('success', 'User added');
            
        }                
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usr = User::find($id);
        return View::make('user.show')
            ->with('usr', $usr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   /* public function edit($id)
    {
        $contrydata = DB::table('tbl_country as c')
        ->select('c.*')
        ->where('status','=','1')
        ->get();
        
        $usr = User::find($id);

        $c_id = $usr['country_id'];

        $branchdata = DB::table('tbl_branch as b')
        ->select('b.*')
        ->where('countryId','=',$c_id)
        ->get();

        $User = User::where('id','!=',$id)->pluck('name','id');
        $User->prepend(trans('adminlte_lang::message.chooseOpt'),'0');
        $roles = \App\Model\Role::get()->pluck('name', 'id')->prepend('--Select Role--', '');
        return View::make('admin_user.edit')
            ->with(array('usr'=> $usr,'id'=>$id,'contrydata'=>$contrydata,'branchdata'=>$branchdata, 'roles' => $roles));
    }
*/
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = array(
            'name'       => 'required',
            //'email' => 'required|email|unique:users,email',
            'phone' => 'required',
            'country' => 'required',
            'role' => 'required'
            // 'usrImage' => 'required'         
        );
        $message = array(
        'name.required' =>  trans('adminlte_lang::message.name_is_required'),
        //'email.required' =>  trans('adminlte_lang::message.email_is_required'),
        'phone.required' =>  trans('adminlte_lang::message.phone_is_required'),
        'country.required' =>  trans('adminlte_lang::message.country_is_required'),
        'role.required' =>  trans('adminlte_lang::message.role_is_required')
        // 'usrImage.required' =>  trans('adminlte_lang::message.usrImage_is_required')
        );
        $validator = Validator::make(Input::all(), $rules,$message);
       
        if( $validator->fails() ){
            return redirect()->back()->withInput()
                             ->with('errors', $validator->errors() );
        }
        $admin_user = User::find($id);
        // echo $request->hasFile('usrImage');exit;
        if($request->hasFile('usrImage')){
            $file = $request->file('usrImage');
            $destination_path = folderArray(5);
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $file->move($destination_path, $filename);            
            $admin_user->logo = $filename;
        }
        // echo $request->input('country');exit;
        $this->syncPermissions($request, $admin_user);

        $admin_user->name = $request->input('name');
        $admin_user->phone = $request->input('phone');
        $admin_user->country_id = $request->input('country');
        $admin_user->branch_id = $request->input('branch');
        $admin_user->role = $request->input('role');
        $admin_user->role_id = $request->input('role_id');

        $admin_user->status = $request->input('status');
        $admin_user->deviceType = '0';
        if($request->input('password') != ""){
            $password = $request->input('password'); 
            // password is form field     
            $hashed = Hash::make($password);
            $admin_user->password = $hashed;
        }
        $admin_user->save();

        return redirect('admin_user')->with('success', trans('adminlte_lang::message.recordEdit'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usr = User::find($id);
        $usr->delete();
        return redirect('/admin_user')->with('success', trans('adminlte_lang::message.recordDelete'));
    }
    public function getbranch(){
        $country_id = Input::get('country_id');
        $branch = DB::table('tbl_branch')->where('countryId',$country_id)->get()->toArray();
        return json_encode($branch,true);
    }

    private function syncPermissions(Request $request, $user)
    {
        // Get the submitted roles
        $roles = $request->get('role_id', []);
        $permissions = $request->get('permissions', []);

        // Get the roles
        $roles = Role::find($roles);

        // check for current role changes
        if( ! $user->hasAllRoles( $roles ) ) { 
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else { 
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);
        return $user;
    }

    public function status_update($id, $status) {
        $getUser = User::find($id);
        $getUser->status = $status;
        $getUser->save();
        return redirect()->back()->with('success', 'Status updated successfully');
    }
}


