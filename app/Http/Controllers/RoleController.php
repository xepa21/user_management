<?php

namespace App\Http\Controllers;

use App\Authorizable;
use App\Permission;
use App\Role;
use App\User;
use App\Http\Controllers\Controller;



use Illuminate\Http\Request;

class RoleController extends Controller
{
    // use Authorizable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
/*        $permission = new Permission(['name' => 'edit-new']);
$permission->save();

$role = Role::find(1);
$role->save();
// dd($role);
$role->givePermissionTo($permission);
User::find(463)->assignRole($role);

if (User::find(463)->can('edit-new')){
            return 'ok';
}*/
/*$permission = Permission::get();
$role = Role::find(1);
$role->givePermissionTo($permission);*/
// User::find(1)->assignRole($role); 

/*$permission = Permission::get();
$role = Role::find(2);

$role->givePermissionTo($permission);
User::find(1)->assignRole($role);*/

// if (User::find(1)->can('add-admin-user')){
//             return 'ok';exit;
// }

        $roles = Role::all();
        $permissions = Permission::all();

        return view('role.index', compact('roles', 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, ['name' => 'required|unique:roles']);

        if( Role::create($request->only('name')) ) {
            // flash('Role Added');
        }

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($role = Role::findOrFail($id)) {
            // admin role has everything
            /*if($role->name === 'Admin') {
                $role->syncPermissions(Permission::all());
                return redirect()->route('user_role.index');
            }*/
            // dd($request->get('permissions', []));
            $permissions = $request->get('permissions', []);
            $role->name = $request->role_name;
            $role->save();
            $role->syncPermissions($permissions);

            // flash( $role->name . ' permissions has been updated.');
        } else {
            // flash()->error( 'Role with id '. $id .' note found.');
        }

        return redirect()->route('user_role.index');
    }
}
