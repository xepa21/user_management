<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\FAQs;
use View;


class FAQController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

   public function index(Request $request)
    {
        $faqs = FAQs::where('deleted_at', NULL)->paginate(5); 
        $categories = Category::where('deleted_at', NULL)->where('status', 1)->pluck('name','id')->prepend('Select', '')->toArray();     
        return View::make('faqs/show', array('faqs' => $faqs, 'categories' => $categories));
    }

    function fetch_data(Request $request)
    {
     if($request->ajax())
     { 
      $faqs = FAQs::where('deleted_at', NULL)->paginate(5);
      return view('faqs/list', compact('faqs'))->render();
     }
    }

    function search($search)
    {
        // dd($search);
        $faqs = FAQs::leftJoin('category as c', 'c.id', '=', 'faq.category_id')->where('faq.title', 'like', '%'.$search.'%')->orWhere('faq.description', 'like', '%'.$search.'%')->orWhere('c.name', 'like', '%'.$search.'%')->paginate(5);
        // dd($faqs);
      return view('faqs/list', array('faqs' => $faqs))->render();
     
    }

    public function store(Request $request) {
    	$insertArray = $request->all();
    	// dd($request);
    	$filename= '';
    	if($request->hasFile('image')) {           
            $uploadPath = public_path() . '/uploads/';
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $file->move($uploadPath, $filename);
        }       
        
        
        $f_id = $request->f_id;
        if($f_id == '') {
        $addFaq = new FAQs;
        $addFaq->category_id = $request->category_id;
        $addFaq->title = $request->title;
        $addFaq->description = $request->description;
        $addFaq->image = $filename;
        $addFaq->save();
    	} else {
    		$faq  = FAQs::findOrFail($f_id);
            if(!$request->hasFile('image')) {
                $filename = $faq->image;
            } else {
                $filename = $filename;
            }
    		$editFaq = FAQs::find($f_id);
    		$editFaq->category_id = $request->category_id;
	        $editFaq->title = $request->title;
	        $editFaq->description = $request->description;
	        $editFaq->image = $filename;
	        $editFaq->save();
    	}

        $faqs = FAQs::where('deleted_at', NULL)->paginate(5);
        return view('faqs.list', compact('faqs'))->render();
    }

    public function edit($id) {
    	$getFaq = FAQs::find($id);
    	return $getFaq;
    }

    public function destroy($id) {
    	$faq = FAQs::find($id);
    	$faq->deleted_at = now();
    	$faq->save();
    	$faqs = FAQs::paginate(5);
    	return view('faqs.list', compact('faqs'))->render();
    }
}
