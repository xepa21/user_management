<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FAQs extends Model
{
    protected $fillable = [
        'category_id', 'title', 'description', 'image', 'deleted_at', 'created_at', 'updated_at'
    ];

    protected $table = "faq";
}
